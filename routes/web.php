<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('dashboard');
})->middleware('auth');

//Users pages
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/club/new', 'ClubController@new')->name('newClubPage');
Route::get('/customer/new', 'CustomerController@new')->name('newCustomerPage');
Route::get('/music/new', 'MusicController@new')->name('newMusicPage');
Route::get('/club/{id}/startParty', 'ClubController@startParty')->name('clubStartParty');


//Entities managing
Route::post('/club', 'ClubController@create')->name('newClub');
Route::post('/music', 'MusicController@create')->name('newMusic');
Route::post('/customer', 'CustomerController@create')->name('newCustomer');
Route::delete('/club/{id}', 'ClubController@delete')->name('removeClub');
Route::delete('/music/{id}', 'MusicController@delete')->name('removeMusic');
Route::delete('/customer/{id}', 'CustomerController@delete')->name('removeCustomer');

//(music/customer) management in a club
Route::post('/club/{clubId}/music', 'ClubController@addMusic')->name('clubAddMusic');
Route::delete('/club/music', 'ClubController@removeMusic')->name('clubRemoveMusic');
Route::post('/club/{clubId}/customer', 'ClubController@addCustomer')->name('clubAddCustomer');
Route::delete('/club/customer', 'ClubController@removeCustomer')->name('clubRemoveCustomer');

//Music status management
Route::patch('/club/music/status', 'ClubCustomer@changeMusicStatus')->name('clubChangeMusicStatus');
Auth::routes();
