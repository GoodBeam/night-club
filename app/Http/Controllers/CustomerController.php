<?php

namespace App\Http\Controllers;

use App\Club;
use App\Customer;
use App\Providers\CustomerCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function new()
    {
        return view('create.customer', ['clubs' => Club::all()]);
    }

    public function create(Request $request)
    {
        $customers = $request->post('customer');
        DB::transaction(function () use ($customers) {
            foreach ($customers as $value) {
                $customer = new Customer();
                $customer->name = $value['name'];
                $customer->gender = $value['gender'];
                $customer->club_id = $value['club_id'];
                $customer->save();
                event(new CustomerCreated($customer));
            }
        });

        return redirect()->route('dashboard');
    }
}
