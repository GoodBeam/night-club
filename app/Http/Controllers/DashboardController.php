<?php

namespace App\Http\Controllers;

use App\Club;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = [
            'clubs' => Club::all(),
            'genres' => MusicController::GENRES
        ];
        return view('dashboard', $data);
    }
}
