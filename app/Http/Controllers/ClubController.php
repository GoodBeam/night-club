<?php

namespace App\Http\Controllers;

use App\Club;
use App\Customer;
use App\Music;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClubController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function new()
    {
        return view('create.club');
    }

    public function create(Request $request)
    {
        $clubs = $request->post('club');
        DB::transaction(function () use ($clubs) {
            foreach ($clubs as $item) {
                $club = new Club();
                $club->name = $item['name'];
                $club->save();
            }
        });

        return redirect()->route('newMusicPage');
    }

    public function startParty(int $id)
    {
        $music_id = (request()->get('music_id')) ?? 1;
        $data = [
            'club' => Club::findOrFail($id),
            'music' => Music::findOrFail($music_id),
            'genres' => MusicController::GENRES
        ];
        return view('club', $data);
    }
}
