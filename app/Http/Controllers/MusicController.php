<?php

namespace App\Http\Controllers;

use App\Club;
use App\Music;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MusicController extends Controller
{
    const GENRES = [
        'Поденсить',
        'Романтик',
        'Электро-денс',
        'Какая-то там не денс :('
    ];

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function new()
    {
        $data = [
            'clubs' => Club::all(),
            'genres' => self::GENRES
        ];
        return view('create.music', $data);
    }

    public function create(Request $request)
    {
        $tracks = $request->post('music');
        DB::transaction(function() use ($tracks){
            foreach ($tracks as $track){
                $music = new Music();
                $music->name = $track['name'];
                $music->genre = $track['genre'];
                $music->club_id = $track['club_id'];
                $music->save();
            }
        });

        return redirect()->route('newCustomerPage');
    }
}
