<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @Author      - Rustem Akhmetzyanov <goodbeam@yandex.ru>
 * @Description - File of Customer class
 * @package App
 * @property int $id
 * @property string $name
 * @property string $gender
 * @property int $club_id
 */
class Customer extends Model
{
    protected $table = 'customer';

    public function club()
    {
        $this->belongsTo(Club::class, 'id', 'club_id');
    }

    public function music()
    {
        return $this->belongsToMany(Music::class, 'customer_music', 'customer_id', 'music_id');
    }
}
