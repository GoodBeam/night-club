<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @Author      - Rustem Akhmetzyanov <goodbeam@yandex.ru>
 * @Description - File of Music class
 * @package App
 * @property int $id
 * @property string $name
 * @property string $genre
 * @property int $club_id
 */
class Music extends Model
{
    protected $table = 'music';

    public function club(){
        $this->hasOne(Club::class, 'id', 'club_id');
    }

    public function customer(){
        return $this->belongsToMany(Customer::class, 'customer_music', 'music_id', 'customer_id');
    }
}
