<?php

namespace App;

use App\Http\Controllers\MusicController;
use Illuminate\Database\Eloquent\Model;

/**
 * @Author      - Rustem Akhmetzyanov <goodbeam@yandex.ru>
 * @Description - File of Club class
 * @package App
 * @property int $id
 * @property string $name
 */
class Club extends Model
{
    protected $table = 'club';

    private $pairs;

    private $group;

    public function music()
    {
        return $this->hasMany(Music::class, 'club_id');
    }

    public function customers()
    {
        return $this->hasMany(Customer::class, 'club_id', 'id');
    }

    public function getPairs()
    {
        if (empty($this->pairs)) {
            $men = Customer::all()->where('gender', 0);
            $women = Customer::all()->where('gender', 1);
            $pairs = [];
            foreach ($men as $m_key => &$man) {
                if (empty($women)) {
                    break;
                }
                foreach ($women as $w_key => &$woman) {
                    if ($man->music->count() > 0 && $woman->music->count() > 0) {
                        $pairs[] = [$man, $woman];
                    }
                    unset($men[$m_key]);
                    unset($women[$w_key]);
                    break;
                }
            }
            $this->pairs = $pairs;
            $this->group = array_merge($men->toArray(), $women->toArray());
        }

        return $this->pairs;
    }

    public function getGroup()
    {
        if (empty($this->group)) {
            $this->getPairs();
        }

        return $this->group;
    }

    public function isRomantic(int $id)
    {
        return array_search('Романтик', MusicController::GENRES) == $id;
    }
}
