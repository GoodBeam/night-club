<?php

namespace App\Providers;

use App\Music;

class AddMusic
{
    /**
     * @var Music
     */
    private $music;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Music $music)
    {
        $this->music = $music;
    }

    /**
     * Handle the event.
     *
     * @param  CustomerCreated $event
     * @return void
     */
    public function handle(CustomerCreated $event)
    {
        $customer = $event->customer;
        $music = $this->music;
        $musicAmount = $music::all()->count();
        $iMax =  mt_rand(1, $musicAmount);

        for ($i = 0; $i < $iMax; $i++) {
            $customer->music()->attach($music::find(mt_rand(1, $musicAmount)));
        }
    }
}
