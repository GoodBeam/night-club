@extends('layouts.app')
@section('content')
    @if(empty($clubs->count()))
        <div class="jumbotron">
            <h1 class="display-4">У Вас нет созданных клубов</h1>
            <p class="lead">Чтобы немного потусить - нажми кнопку ниже</p>
            <a class="btn btn-outline-primary btn-lg" href="{{ route('newClubPage') }}" role="button">тусить</a>
        </div>
    @endif
    @foreach($clubs as $club)
        <div class="card text-white bg-info">
            <div class="card-header">
                {{ $club->name }}
            </div>
            <div class="card-body">
                @if(empty($club->customers->count()))
                    <h5 class="card-title">Нет посетителей :с</h5>
                    <a href="{{ route('newCustomerPage') }}" class="btn btn-light">Добавить</a>
                @else
                    <h5 class="card-title">Посетители:</h5>
                    <ul>
                        @foreach($club->customers ?? [] as $customer)
                            <li>
                                <b>{{$customer->name}}</b><br>
                                @if(empty($customer->music->count()))
                                    Не умеет танцевать :c
                                @else
                                    Знает треки:
                                    <ul>
                                        @foreach($customer->music as $music)
                                            <li>{{ $music->name }} ({{ $genres[$music->genre] }})</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                    <a href="{{route('clubStartParty', ['id' => $club->id])}}" class="btn btn-light">Начать тусовку</a>
                @endif
            </div>
        </div>
    @endforeach
@endsection