<form method="POST" action="{{ route('newMusic') }}">
    @csrf
    <div id="elements">
        <div class="form-group" id="0">
            <label for="music[0][name]">Что за трек?</label>
            <div class="input-group">
                <input required type="text" class="form-control" name="music[0][name]"
                       placeholder="DJ pes">
                <div class="input-group-append">
                    <button class="btn btn-outline-danger delete" onclick="$(this).parent().parent().parent().remove()">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
        </div>
        <div class="form-group" id="0">
            <label for="music[0][genre]">Какой жанр?</label>
            <div class="input-group">
                <select required class="form-control" name="music[0][genre]">
                    @foreach($genres as $key => $genre)
                        <option value="{{ $key }}">{{ $genre }}</option>
                    @endforeach
                </select>
                <div class="input-group-append">
                    <button class="btn btn-outline-danger delete" onclick="$(this).parent().parent().parent().remove()">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
        </div>
        <div class="form-group" id="0">
            <label for="music[0][club_id]">В каком клубе будет зажигать?</label>
            <div class="input-group">
                <select required class="form-control" name="music[0][club_id]">
                    @foreach($clubs as $club)
                        <option value="{{ $club->id }}">{{ $club->name }}</option>
                    @endforeach
                </select>
                <div class="input-group-append">
                    <button class="btn btn-outline-danger delete" onclick="$(this).parent().parent().parent().remove()">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>
    <button id="copy" type="button" class="btn btn-outline-primary"><i class="fas fa-plus"></i></button>
    <button type="submit" class="btn btn-outline-primary">Сохранить</button>
</form>
<script>
    function script() {
        const script = {
            copyButton: () => {
                $('#copy').click(() => {
                    script.copy();
                })
            },
            copy: () => {
                let form = $('.form-group');
                script.increase($(form[form.length - 3]).clone());
                script.increase($(form[form.length - 2]).clone(), 'genre');
                script.increase($(form[form.length - 1]).clone(), 'club');
            },
            increase: (block, type = 'name') => {
                let i = parseInt(block.attr('id')) + 1,
                    input = (type === 'name' ? 'input' : 'select'),
                    text = 'music[' + i + ']' + (type === 'name' ? '[name]' : (type === 'club' ? '[club_id]' : '[genre]'));
                block.attr({'id': i});
                block.find('label').attr({'for': text});
                block.find(input).attr({'name': text});
                script.paste(block);
            },
            paste: (block) => {
                block.appendTo('#elements')
            },
            init: () => {
                script.copyButton();
            }
        };
        script.init();
    }

    document.addEventListener("DOMContentLoaded", script);
</script>