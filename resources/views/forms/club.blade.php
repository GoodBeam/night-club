<form method="POST" action="{{ route('newClub') }}">
    @csrf
    <div id="elements">
        <div class="form-group" id="0">
            <label for="club[0]['name']">Наименование клуба</label>
            <div class="input-group">
                <input required type="text" class="form-control" name="club[0][name]"
                       placeholder="Первый на районе">
                <div class="input-group-append">
                    <button class="btn btn-outline-danger delete" onclick="$(this).parent().parent().parent().remove()">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>
    <button id="copy" type="button" class="btn btn-outline-primary"><i class="fas fa-plus"></i></button>
    <button type="submit" class="btn btn-outline-primary">Сохранить</button>
</form>
<script>
    function script() {
        const script = {
            copyButton: () => {
                $('#copy').click(() => {
                    script.copy();
                })
            },
            copy: () => {
                script.increase($($('.form-group')[0]).clone());
            },
            increase: (block) => {
                let i = parseInt(block.attr('id')) + 1;
                block.attr({'id': i});
                block.find('label').attr({'for': 'club[' + i + '][name]'});
                block.find('input').attr({'name': 'club[' + i + '][name]'});
                script.paste(block);
            },
            paste: (block) => {
                block.appendTo('#elements')
            },
            init: () => {
                script.copyButton();
            }
        };
        script.init();
    }

    document.addEventListener("DOMContentLoaded", script);
</script>