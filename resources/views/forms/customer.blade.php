<form method="POST" action="{{ route('newCustomer') }}">
    @csrf
    <div id="elements">
        <div class="form-group" id="0">
            <label for="customer[0][name]">Как зовут посетителя?</label>
            <div class="input-group">
                <input required type="text" class="form-control" name="customer[0][name]"
                       placeholder="Мамин тусовщик">
                <div class="input-group-append">
                    <button class="btn btn-outline-danger delete" onclick="$(this).parent().parent().parent().remove()">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
        </div>
        <div class="form-group" id="0">
            <label for="customer[0][club_id]">У вас мальчик или девочка?</label>
            <div class="input-group">
                <select required class="form-control" name="customer[0][gender]">
                        <option value="0">Конечно мальчик</option>
                        <option value="1">Определенно девочка</option>
                </select>
                <div class="input-group-append">
                    <button class="btn btn-outline-danger delete" onclick="$(this).parent().parent().parent().remove()">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
        </div>
        <div class="form-group" id="0">
            <label for="customer[0][club_id]">Где он будет отжигать?</label>
            <div class="input-group">
                <select required class="form-control" name="customer[0][club_id]">
                    @foreach($clubs as $club)
                        <option value="{{ $club->id }}">{{ $club->name }}</option>
                    @endforeach
                </select>
                <div class="input-group-append">
                    <button class="btn btn-outline-danger delete" onclick="$(this).parent().parent().parent().remove()">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
        </div>
    </div>
    <button id="copy" type="button" class="btn btn-outline-primary"><i class="fas fa-plus"></i></button>
    <button type="submit" class="btn btn-outline-primary">Сохранить</button>
</form>
<script>
    function script() {
        const script = {
            copyButton: () => {
                $('#copy').click(() => {
                    script.copy();
                })
            },
            copy: () => {
                let form = $('.form-group');
                script.increase($(form[form.length - 3]).clone());
                script.increase($(form[form.length - 2]).clone(), 'gender');
                script.increase($(form[form.length - 1]).clone(), 'club');
            },
            increase: (block, type = 'name') => {
                let i = parseInt(block.attr('id')) + 1,
                    input = (type === 'name' ? 'input' : 'select'),
                    text = 'customer[' + i + ']' + (type === 'name' ? '[name]' : (type === 'gender' ? '[gender]' : '[club_id]'));
                block.attr({'id': i});
                block.find('label').attr({'for': text});
                block.find(input).attr({'name': text});
                script.paste(block);
            },
            paste: (block) => {
                block.appendTo('#elements')
            },
            init: () => {
                script.copyButton();
            }
        };
        script.init();
    }

    document.addEventListener("DOMContentLoaded", script);
</script>