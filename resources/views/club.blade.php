@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <h1 class="display-4">Тусовка в клубе {{ $club->name }}</h1>
        <p class="lead">Сейчас играет трек {{ $music->name }} ({{ $genres[$music->genre] }})</p>
        <hr class="my-4">
        <ul>
            <li>
                Танцуют:
                @if($club->isRomantic($music->genre))
                    -
                @else
                    <ul>
                        @foreach($club->customers as $customer)
                            @foreach($customer->music as $customerMusic)
                                @if($customerMusic->genre === $music->genre)
                                    <li>{{ $customer->name }} ({{$customer->gender}})</li>
                                    @break
                                @endif
                            @endforeach
                        @endforeach
                    </ul>
                @endif
            </li>
            <li>
                Стоят:
                <ul>
                    @foreach($club->customers as $customer)
                        @if(!in_array($music->genre, $customer->music->pluck('genre')->toArray()))
                            <li>{{ $customer->name }} ({{$customer->gender}})</li>
                        @endif
                    @endforeach
                </ul>
            </li>
            <li>
                Пары:
                @if($club->isRomantic($music->genre))
                    @if(count($club->getPairs()) === 0)
                        -
                    @endif
                    <ul>
                        @foreach($club->getPairs() as $pair)
                            <li>{{ $pair[0]['name'] }} ({{$pair[0]['gender']}}) + {{ $pair[1]['name'] }}
                                ({{$pair[1]['gender']}})
                            </li>
                        @endforeach
                    </ul>
                @else
                    -
                @endif
            </li>
            <li>
                Группа:
                @if($club->isRomantic($music->genre))
                    @if(count($club->getGroup()) === 0)
                        -
                    @endif
                    <ul>
                        @foreach($club->getGroup() as $member)
                            <li>{{ $member['name'] }} ({{$member['gender']}})</li>
                        @endforeach
                    </ul>
                @else
                    -
                @endif
            </li>
        </ul>
        <form>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Хей, диджей, смени трек</label>
                <select onchange="window.location.search = '?music_id=' + this.value" class="form-control" id="exampleFormControlSelect1">
                    @foreach($club->music as $music)
                        <option value="{{ $music->id }}" @if(request()->get('music_id') == $music->id) selected @endif>{{ $music->name }} ({{ $genres[$music->genre] }})</option>
                    @endforeach
                </select>
            </div>
        </form>
        <a class="btn btn-primary btn-lg" href="{{ route('dashboard') }}" role="button">Потусить в другом клубе</a>
    </div>
@endsection